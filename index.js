import SpotifyWebApi from 'spotify-web-api-node'
import express from 'express'

const spotifyApi = new SpotifyWebApi({
  clientId: 'bfc6942cfd494aa2b18c8b2eeed064ba',
  clientSecret: '3b4ebb441e5a4e9191d5d936afd09bd7',
  redirectUri: 'http://localhost:3000/callback'
});

const scopes = ['user-read-private', 'user-read-email', 'user-modify-playback-state', 'user-read-currently-playing', 'user-read-playback-state','user-library-read']

const code = 'AQCU6x3wRf6Or-_3ZpoLkvLfk9_6JQZepZ0AHVLpfazT6y0olCY31S9GHtIhhaURk4WYqTTdONDY3sLvjSRN3q5Dny7OVO5mbWoej-n8Z9F5vxOhJ8VrDbQNFgek27B4U6wsSsr9Ea7cKCe_U0JagmBUpKzbKjQoZUF9Le5EbgiI_vg0Gp64RLY2aGWdO7zkwCMRHKNiarzqkntDv__dnmwD6gPQWTf9W2BKyovsepgDLZTU5OguLQ'
const state = 'fjdsgasgslakjs'


const app = express();

app.get('/login', (req,res)=>{
  res.redirect(spotifyApi.createAuthorizeURL(scopes, state))
})
let id = ''

app.get('/callback', (req, res) => {

  let code = req.query.code
  spotifyApi
    .authorizationCodeGrant(code)
    .then((data) => {
      console.log('The access token expires in ' + data.body['expires_in']);
      console.log('The access token is ' + data.body['access_token']);

      // Save the access token so that it's used in future calls
      spotifyApi.setAccessToken(data.body['access_token']);
      spotifyApi.setRefreshToken(data.body['refresh_token']);
      res.sendStatus(200);
    })
    .catch((err) => {
      console.log('Something went wrong: ', err);
    });
});

app.get('/devices',(req,res)=>{
   spotifyApi.getMyDevices()
    .then((data) => {
      // Output items
      res.json( data.body.devices[0]);
      console.log(id);
    })
    .catch(error=>{
      console.log(error);
      res.sendStatus(500);
    });
});

app.get('/play',(req,res)=>{
  spotifyApi.searchTracks(req.query.track)
    .then(info=>{
      console.log(info)
      //console.log(id)
      let song = info.body.tracks.items[0]
      console.log(song);
      return spotifyApi.play({uris:[song.uri],device_id:req.query.device_id})
    })
    .then(result=>{
      res.json(result)
    })
    .catch(error=>{
      console.log(error);
      res.send(500);
    })
})

const PORT = 3000;

app.listen(PORT, () => {
  console.log(`server running on port ${PORT}`)
});
